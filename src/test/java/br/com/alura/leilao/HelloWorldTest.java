package br.com.alura.leilao;

import static org.junit.jupiter.api.Assertions.assertTrue;

import br.com.alura.leilao.dao.LeilaoDao;
import br.com.alura.leilao.model.Leilao;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class HelloWorldTest {

  @Test
  void hello() {
    LeilaoDao leilaoDao = Mockito.mock(LeilaoDao.class);
    List<Leilao> list = leilaoDao.buscarTodos();
    assertTrue(list.isEmpty());
  }

}
