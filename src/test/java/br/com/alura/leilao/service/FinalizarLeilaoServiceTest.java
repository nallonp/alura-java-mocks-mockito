package br.com.alura.leilao.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import br.com.alura.leilao.dao.LeilaoDao;
import br.com.alura.leilao.model.Lance;
import br.com.alura.leilao.model.Leilao;
import br.com.alura.leilao.model.Usuario;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class FinalizarLeilaoServiceTest {

  @Mock
  private EnviadorDeEmails enviadorDeEmails;
  @Mock
  private LeilaoDao leilaoDao;
  @InjectMocks
  private FinalizarLeilaoService finalizarLeilaoService;

  @Test
  void deveriaFinalizarLeilao() {
    List<Leilao> leiloes = leiloes();
    Mockito.when(this.leilaoDao.buscarLeiloesExpirados()).thenReturn(leiloes);
    this.finalizarLeilaoService.finalizarLeiloesExpirados();
    Leilao leilao = leiloes.get(0);
    assertTrue(leilao.isFechado());
    assertEquals(new BigDecimal("900"), leilao.getLanceVencedor().getValor());
    Mockito.verify(leilaoDao).salvar(leilao);
  }

  @Test
  void deveriaEnviarEmailParaOVencedor() {
    List<Leilao> leiloes = leiloes();
    Mockito.when(this.leilaoDao.buscarLeiloesExpirados()).thenReturn(leiloes);
    this.finalizarLeilaoService.finalizarLeiloesExpirados();
    Leilao leilao = leiloes.get(0);
    Lance lanceVencedor = leilao.getLanceVencedor();
    Mockito.verify(enviadorDeEmails).enviarEmailVencedorLeilao(lanceVencedor);
  }

  @Test
  void naoDeveriaEnviarEmailParaOVencedorCasoHouvesseErroAoEncerrarLeilao() {
    List<Leilao> leiloes = leiloes();
    Mockito.when(this.leilaoDao.buscarLeiloesExpirados()).thenReturn(leiloes);
    Mockito.when(this.leilaoDao.salvar(Mockito.any())).thenThrow(RuntimeException.class);
    try {
      this.finalizarLeilaoService.finalizarLeiloesExpirados();
      Mockito.verifyNoInteractions(enviadorDeEmails);
    } catch (Exception ignored) {
    }
  }

  private List<Leilao> leiloes() {
    List<Leilao> lista = new ArrayList<>();
    Leilao leilao = new Leilao("Celular", new BigDecimal("500"), new Usuario("Fulano"));

    Lance primeiro = new Lance(new Usuario("Beltrano"), new BigDecimal("600"));
    Lance segundo = new Lance(new Usuario("Ciclano"), new BigDecimal("900"));

    leilao.propoe(primeiro);
    leilao.propoe(segundo);

    lista.add(leilao);
    return lista;
  }
}