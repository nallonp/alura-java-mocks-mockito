package br.com.alura.leilao.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import br.com.alura.leilao.dao.PagamentoDao;
import br.com.alura.leilao.model.Lance;
import br.com.alura.leilao.model.Leilao;
import br.com.alura.leilao.model.Pagamento;
import br.com.alura.leilao.model.Usuario;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class GeradorDePagamentoTest {

  @Mock
  private PagamentoDao pagamentoDao;

  @Captor
  private ArgumentCaptor<Pagamento> pagamentoCaptor;

  @Mock
  private Clock clock;

  @InjectMocks
  private GeradorDePagamento geradorDePagamento;

  @Test
  void deveriaCriarPagamentoParaOVencedorDoLeilao() {
    Leilao leilao = leilao();
    Lance lanceVencedor = leilao.getLanceVencedor();
    LocalDate data = LocalDate.of(2020, 12, 7);
    Instant instant = data.atStartOfDay(ZoneId.systemDefault()).toInstant();
    Mockito.when(clock.instant()).thenReturn(instant);
    Mockito.when(clock.getZone()).thenReturn(ZoneId.systemDefault());
    geradorDePagamento.gerarPagamento(lanceVencedor);
    Mockito.verify(pagamentoDao).salvar(pagamentoCaptor.capture());
    Pagamento pagamento = this.pagamentoCaptor.getValue();
    assertEquals(data.plusDays(1), pagamento.getVencimento());
    assertEquals(lanceVencedor.getValor(), pagamento.getValor());
    assertFalse(pagamento.getPago());
    assertEquals(lanceVencedor.getUsuario(), pagamento.getUsuario());
    assertEquals(leilao, pagamento.getLeilao());
  }

  private Leilao leilao() {
    Leilao leilao = new Leilao("Celular", new BigDecimal("500"), new Usuario("Fulano"));

    Lance lance = new Lance(new Usuario("Ciclano"), new BigDecimal("900"));

    leilao.propoe(lance);
    leilao.setLanceVencedor(lance);

    return leilao;
  }
}