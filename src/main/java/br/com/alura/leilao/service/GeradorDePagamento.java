package br.com.alura.leilao.service;

import br.com.alura.leilao.dao.PagamentoDao;
import br.com.alura.leilao.model.Lance;
import br.com.alura.leilao.model.Pagamento;
import java.time.Clock;
import java.time.DayOfWeek;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GeradorDePagamento {

  private final PagamentoDao pagamentoDao;
  private final Clock clock;

  @Autowired
  public GeradorDePagamento(PagamentoDao pagamentoDao, Clock clock) {
    this.pagamentoDao = pagamentoDao;
    this.clock = clock;
  }

  public void gerarPagamento(Lance lanceVencedor) {
    LocalDate vencimento = LocalDate.now(this.clock).plusDays(1);
    Pagamento pagamento = new Pagamento(lanceVencedor, proximoDiaUtil(vencimento));
    this.pagamentoDao.salvar(pagamento);
  }

  private LocalDate proximoDiaUtil(LocalDate dataVencimento) {
    DayOfWeek dayOfWeek = dataVencimento.getDayOfWeek();
    if (dayOfWeek.equals(DayOfWeek.SATURDAY)) {
      return dataVencimento.plusDays(2);
    }
    if (dayOfWeek.equals(DayOfWeek.SUNDAY)) {
      return dataVencimento.plusDays(1);
    }
    return dataVencimento;
  }

}
